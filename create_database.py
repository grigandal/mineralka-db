from models import db, Answer, Question, User, Element, UserAnswer


def create_database():
    try:
        db.connect(reuse_if_open=True)
    except Exception as e:
        print(e)
    else:
        db.create_tables([User, Element, Question, Answer, UserAnswer])


if __name__ == '__main__':
    create_database()
