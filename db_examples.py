from models import Answer, Element, Question, UserAnswer, db, User
from time import time
import random


def create_user():
    print('\n\n\nСоздание пользователя')

    # Для примера чтоб был уникальный telegram_id возьмем текущее время в формате timestamp
    telegram_id = int(time() * 1000)

    age = random.randint(10, 30)
    sex = random.randint(0, 2)

    u = User(telegram_id=telegram_id, age=age, sex=sex)
    u.save()
    print(u)  # Напечатается id (PK)
    print(u.telegram_id, u.age, u.sex, u.id)  # Напечатаются поля пользователя
    print(u.__dict__['__data__'])  # Напечатается dict с полями пользователя
    return u


def create_question():
    print('\n\n\nСоздание вопроса:')

    # Если вдруг нет химических элементов - создаем
    if not Element.select().count():
        element = Element(name='Zn')
        element.save()
    else:
        element = Element.select()[0]

    # Создаем вопрос
    question = Question(
        element=element, text=f'Вопрос {Question.select().count() + 1}')
    question.save()

    print(question.__dict__['__data__'])

    # Создаем варианты ответов
    for i in range(random.randint(2, 6)):
        answer = Answer(
            text=f'Ответ {i + 1} на вопрос "{question.text}"', grade=i, question=question)
        answer.save()
        print(answer.__dict__['__data__'])

    return question


# Выбор всех вариантов ответа по конкретному вопросу
def get_question_answers(question=None):
    if question is None:
        question = Question.select()[0]
    answers = Answer.select().where(Answer.question == question)
    for answer in answers:

        q = answer.question  # Сразу можно получить объект вопроса для данного варианта ответа
        print(answer.__dict__['__data__'])

    return answers


def create_user_answers():  # Создание ответов пользователя
    print('\n\nСоздание ответов пользователя')
    if not User.select().count():
        user = create_user()
    else:
        user = User.select()[0]

    if Question.select().count() < 10:
        for i in range(10):
            question = create_question()
    else:
        question = random.choice([q for q in Question.select()])

    answers = Answer.select().where(Answer.question == question)

    for i in range(10):
        print(question.text)

        # Если уже существует ответ на вопрос от данного пользователя - ниче не делаем
        if (UserAnswer
            .select()
            .join(Answer)
            .where(Answer.question == question, UserAnswer.user == user)
                .count()):

            print(f'Пользователь уже дал ответ на вопрос "{question.text}"')

        else:
            ua = UserAnswer(
                user=user, answer=random.choice([a for a in answers]))
            ua.save()
            print(ua.__dict__['__data__'])

        question = random.choice([q for q in Question.select()])


def get_user_answers():

    user = User.select()[0]
    print(f'\n\nПодсчет баллов для пользователя {user.telegram_id}')

    user_answers = UserAnswer.select().where(UserAnswer.user == user)
    print('Сумма баллов:', sum([ua.answer.grade for ua in user_answers]))
    return user_answers


if __name__ == '__main__':
    create_user()
    create_question()
    get_question_answers()
    create_user_answers()
    get_user_answers()
