# БД для телеграм-бота "Mineralka"

## ER-диаграмма БД

![MineralkaDB](./img/MineralkaDB.png)

## Структура репозитория

### Основные файлы

1. [models.py](./models.py) - код, описывающий таблицы бд (интерпретация ER-диаграммы в нотации ORM `peewee`)
2. [create_database.py](./create_database.py) - код, создающий бд и таблицы
3. [db_examples.py](./db_examples.py) - примеры на запись и чтение БД с помощью ORM `peewee`

### Вспомогательные файлы

-   [Pipfile](./Pipfile) - служебный файл со списком зависимостей для `pipenv`

## Установка и создание БД

> Внимание! _При выполнении всех `.py` файлов, никакая другая программа не должна использовать созданную далее БД_

### Вариант 1 (без использования `pipenv`)

1. Установить `python3` и `pip`
2. Выполнить команду в терминале (установка ORM `peewee`):
    ```bash
    pip install peewee
    ```
3. Запустить `create_database.py`:
    ```bash
    python create_database.py
    ```

### Вариант 2

> Обязателен `python3.9`. Если необходимо использовать другую версию - поменять её в `Pipfile`

1. Установить `python3` и `pip`
2. Установить `pipenv`
    ```bash
    python -m pip install pipenv
    ```
3. Установить зависимости (автоматически подтянутся из `Pipfile`)
    ```bash
    pipenv install
    ```
4. Запустить `create_database.py` внутри созданного виртуального окружения:

    ```bash
    pipenv run python create_database.py
    ```

    или

    Активируем виртуальное окружение:

    ```bash
    pipenv shell
    ```

    Выполняем в активированном виртуальном окружении

    ```bash
    python create_database.py
    ```

После выполненных действий в обоих вариантах создастся БД с именем `mineralka_new.db`
