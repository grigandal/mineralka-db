from peewee import CharField, ForeignKeyField, IntegerField, Model, TextField, SqliteDatabase

# У всех моделей автоматически создается также поле id: PRIMARY KEY AUTOINCREMENT

db = SqliteDatabase('mineralka_new.db')  # Наша бд

# Базовый класс таблицы с прописанной бд, к которой она принадлежит


class BaseModel(Model):
    class Meta:
        database = db


# -------------------------------Таблицы--------------------------------


class User(BaseModel):  # Пользователь
    SEX_CHIOCES = (
        (0, 'Мужской'),
        (1, 'Женский'),
        (2, 'Не указан')
    )

    telegram_id = IntegerField(unique=True)
    sex = IntegerField(null=True, choices=SEX_CHIOCES, default=0)
    age = IntegerField(null=True)


class Element(BaseModel):  # Хим. элемент
    name = CharField()


class Question(BaseModel):  # Вопрос
    text = TextField()
    element = ForeignKeyField(Element)


class Answer(BaseModel):  # Вариант ответа
    text = TextField()
    question = ForeignKeyField(Question)
    grade = IntegerField()


class UserAnswer(BaseModel):  # Связка варианта ответа и пользователя
    user = ForeignKeyField(User)
    answer = ForeignKeyField(Answer)
